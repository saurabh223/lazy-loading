
import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import './Load.css'

const Home = lazy(() => import('./Home'));
const Login = lazy(() => import('./Login'));
const Table =lazy(() => import('./Table'))

const Lazy = () => (
  <Router>
      <Link to="/">Home</Link><Link className='m-4' to="/Login">Login</Link><Link to="/Table">Table</Link>
    <Suspense fallback={<div><div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>}>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route exact path="/Login" element={<Login />} />
        <Route exact path="/Table" element={<Table />} />
      </Routes>
    </Suspense>
  </Router>
);

export default Lazy;